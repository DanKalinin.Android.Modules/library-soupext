package library.soupext;

import android.content.Context;
import androidx.startup.Initializer;
import java.util.ArrayList;
import java.util.List;
import library.glibext.GeInit;
import library.glibnetworkingext.GneInit;
import library.gnutlsext.TlsInit;
import library.java.lang.LjlObject;

public class SoeInit extends LjlObject implements Initializer<Void> {
    @Override
    public Void create(Context context) {
        System.loadLibrary("library-soupext");
        i18n(context.getFilesDir().toString());
        po();
        return null;
    }

    @Override
    public List<Class<? extends Initializer<?>>> dependencies() {
        ArrayList<Class<? extends Initializer<?>>> ret = new ArrayList<>();
        ret.add(GeInit.class);
        ret.add(TlsInit.class);
        ret.add(GneInit.class);
        return ret;
    }

    public static native void i18n(String directory);
    public static native void po();
}
