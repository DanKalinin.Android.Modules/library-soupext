//
// Created by Dan on 18.10.2021.
//

#include "SoeInit.h"

jint JNI_OnLoad(JavaVM *vm, gpointer reserved) {
    soe_init();

    return JNI_VERSION_1_2;
}

void JNI_OnUnload(JavaVM *vm, gpointer reserved) {

}

JNIEXPORT void JNICALL Java_library_soupext_SoeInit_i18n(JNIEnv *env, jclass class, jstring directory) {
    gchar *sdkDirectory = GeJNIEnvGetStringUTFChars(env, directory, NULL);
    soe_init_i18n(sdkDirectory);
    GeJNIEnvReleaseStringUTFChars(env, directory, sdkDirectory);
}

JNIEXPORT void JNICALL Java_library_soupext_SoeInit_po(JNIEnv *env, jclass class) {
    soe_init_po();
}
